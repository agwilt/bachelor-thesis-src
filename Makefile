CXX = clang++
#CXXFLAGS = -Wall -Wextra -g -pedantic -std=c++17 -fsanitize=undefined -march=native -mtune=native
CXXFLAGS = -Wall -Wextra -Ofast -pedantic -std=c++17 -march=native -mtune=native

DEPS = Makefile
OBJ = obj

$(OBJ)/%.o: %.C %.H $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: enumerate_placements

enumerate_placements: enumerate_placements.C $(OBJ)/Graph.o $(OBJ)/DoubleTrail.o $(OBJ)/PlacementEnumerator.o $(DEPS)
	$(CXX) $(CXXFLAGS) enumerate_placements.C $(OBJ)/Graph.o $(OBJ)/DoubleTrail.o $(OBJ)/PlacementEnumerator.o -o enumerate_placements

.PHONY: clean bin_clean obj_clean
clean: bin_clean obj_clean
bin_clean:
	rm -fv enumerate_placements
obj_clean:
	rm -rfv $(OBJ)/*
