#!/usr/bin/env python3

import sys
from xml.dom import minidom
import xml.etree.ElementTree as ET

devices = {'N' : 0, 'P' : 1}

class Fet:
    def __init__(self, name, gate, source, drain, size, device):
        self.name = name
        self.gate = gate
        self.source = source
        self.drain = drain
        self.size = size
        self.device = device # should be 0 (N) or 1 (P)
        assert self.device in [0,1]

    def __repr__(self):
        return "(%s, %s, %s, %s, %d, %d, %d)" % (self.name, self.gate, self.source, self.drain, self.size, self.device)

class Netlist:
    def __init__(self):
        self._names = []

    def containts_net(self, name):
        return (name in self._names)

    def add_net(self, name):
        assert name not in self._names
        self._names.append(name)
        return len(self._names) # return idx

    def net_idx_or_add(self, name):
        if name in self._names:
            return self._names.index(name) + 1
        else:
            return self.add_net(name)

    def net_idx(self, name):
        return self._names.index(name) + 1

    def net_name(self, idx):
        return self._names[idx - 1]

    def __len__(self):
        return len(self._names)


def fets_to_graphs(fets):
    lists_of_lines = [[], []]

    graph_netlists = [Netlist(), Netlist()] # N,P
    label_netlist = Netlist()

    for fet in fets:
        netlist = graph_netlists[fet.device]
        lines = lists_of_lines[fet.device]
        gate = label_netlist.net_idx_or_add(fet.gate)
        start = netlist.net_idx_or_add(fet.source)
        end = netlist.net_idx_or_add(fet.drain)

        separator = "-" if (fet.size % 2) else ">"
        if fet.size == 1:
            lines.append("%d - %d %d" % (start, end, gate))
        else:
            lines.append("%d %s %d %d" % (start, separator, netlist.add_net(fet.name + "_1"), gate))
            for i in range(2, fet.size):
                lines.append("%d - %d %d" % (netlist.net_idx(fet.name + "_" + str(i-1)), separator, netlist.add_net(fet.name + "_" + str(i)), gate))
            lines.append("%d %s %d %d" % (netlist.net_idx(fet.name + "_" + str(fet.size - 1)), separator, end, gate))

    for stack in [0, 1]:
        lists_of_lines[stack].insert(0, str(len(graph_netlists[stack]) + 1))

    return lists_of_lines

def write_graph_files(name, fets):
    filenames = [name + "_N", name + "_P"]
    files = [open(filenames[stack], "w") for stack in [0,1]]
    lists_of_lines = fets_to_graphs(fets)
    for stack in [0,1]:
        for line in lists_of_lines[stack]:
            print(line, file=files[stack])

def fets_from_xml(input_file):
    tree = ET.parse(input_file)
    for fet in tree.find("netlist").findall("fet"):
        yield Fet(fet.attrib['name'],
                  fet.find('gateNet').text,
                  fet.find('sourceNet').text,
                  fet.find('drainNet').text,
                  int(fet.find('numFingers').text),
                  devices[fet.find('device').text])

def example_fets():
    return [Fet("N1", "input", "VDD", "middle", 1, 1, 0), Fet("N2", "middle", "VDD", "output", 2, 1, 0),
            Fet("P1", "input", "middle", "GND", 1, 1, 1), Fet("P2", "middle", "output", "GND", 2, 1, 1)]

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s instance_name [input_file]" % sys.argv[0], file=sys.stderr)
        exit(1)

    input_file = sys.stdin if (len(sys.argv) == 2) else sys.argv[2]

    write_graph_files(sys.argv[1], fets_from_xml(input_file))
