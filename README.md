# Programs

## enumerate\_placements
 * `./enumerate_placements a[ll] G H width max_misalignment start_G start_H [f]`: Enumerate *all* pairs of eulerian trails in G
and H with misalignment at most `max_misalignment`.
 * `./enumerate_placements o[pt] G H width max_misalignment start_G start_H [f]`: Enumerate all max-aligned pairs of eulerian
trails in G and H (with misalignment at most `max_misalignment`).
### Arguments:
 * `G`, `H`: Undirected Graphs in the standard AlMa1 format
 * `width`: Length of "euler trail". If set to `-`, this evaluates to min(|E(G)|, |E(H)|). If the width is larger than
the number of edges in some graph, then the "euler trail" loops at some vertices.
 * `max_misalignment`: Maximum misalignment between pairs of eulerian trails. If set to `-`, all possible (or all
optimal) pairs are returned.
 * `start_G`, `start_H`: Optional fixed start nodes for G and H. If set to `-`, all sensible start nodes are tried.
 * `f`: Forbid gaps of more than 1 PCTrack. This drastically improves performance, at the cost of losing some placements.
### Notes:
 * `G` and `H` are assumed to contain (not necessarily closed) eulerian trails
 * Small single-PCTrack-gaps are described as edges NO\_EDGE from a net to itself
 * Larger ≥2-PCTrack-gaps are described as edges NO\_EDGE from a net (or from NO\_NODE) to NO\_NODE and back again
 * To avoid redundancy, it is forbidden to use any kind of NO\_EDGE after a NO\_EDGE was taken *to* a valid node (net)
 * Note that larger gaps are never (half-) taken as a last "edge", since that would skip too many edges

# TODO:
 * Convert non-eulerian (including disconnected) graphs to eulerian instances
 * Convert schematics to sharing graphs
