#!/usr/bin/env python3

import sys
import random

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: %s n m filename" % sys.argv[0])
        exit(1)
    n = int(sys.argv[1])
    m = int(sys.argv[2])
    filename = sys.argv[3]

    n_file = open(filename + "_n", "w")
    p_file = open(filename + "_p", "w")

    print(n+1, file=n_file)
    print(n+1, file=p_file)

    n_node = 1
    p_node = 1

    n_gates = [random.randint(1,n) for i in range(m)]
    p_gates = n_gates[:]

    random.shuffle(n_gates)
    random.shuffle(p_gates)

    for i in range(m-1):
        n_old_node = n_node
        p_old_node = p_node
        while(n_node == n_old_node):
            n_node = random.randint(2,n)
        while(p_node == p_old_node):
            p_node = random.randint(2,n)
        print("%d %d %d" % (n_old_node, n_node, n_gates[i]), file=n_file)
        print("%d %d %d" % (p_old_node, p_node, p_gates[i]), file=p_file)

    print("%d %d %d" % (n_node, 1, n_gates[m-1]), file=n_file)
    print("%d %d %d" % (p_node, 1, p_gates[m-1]), file=p_file)
