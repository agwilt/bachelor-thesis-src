#!/bin/sh

mkdir -p graphs

printf '' > times.txt

for file in ${1}/*.schematic.xml
do
	cell=`basename ${file/.schematic.xml/}`
	./fets_to_graph.py graphs/${cell} ${file}
	/usr/bin/time -f '%e' -a -o times.txt  -- ./enumerate_placements a graphs/${cell}_{N,P} - 0 - -
done
